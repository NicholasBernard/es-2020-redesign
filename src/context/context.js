import React from 'react'

export const HeaderTitleContext = React.createContext()
export const ThemeContext = React.createContext()
