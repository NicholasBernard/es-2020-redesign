import { navigate } from '@reach/router'

export const ROOT_URL =
  'https://enrichingstudentsapprewriteadmin.azurewebsites.net/v1.0/'

export const handleError = error => {
  if (error.response.status === 401) {
    // 401 Unauthorized - Boot them from the site and nuke the cookie
  } else if (error.response.status > 401 && error.response.status < 500) {
    // WHAT TO DO?!?!?
  } else if (error.response.status >= 500) {
    // 500 and up is a server issue, redirect to page and explain the issue, please patient etc
    navigate('/globalErrorHandling')
  }
}

export const esAuthToken = () => {
  return {
    headers: {
      esauthtoken: 'bob'
    }
  }
}
