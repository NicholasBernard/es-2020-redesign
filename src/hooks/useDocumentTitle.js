import { useEffect } from 'react'

const useDocumentTitle = title => {
  useEffect(() => {
    const setTitle = () => {
      document.title = `Enriching Students - ${title}`
    }
    setTitle()
  }, [title])
}

export default useDocumentTitle
