import { useEffect, useState } from 'react'

const useAddition = (value1, value2) => {
  const [addedValue, setAddedValue] = useState()
  useEffect(() => {
    console.log('in useAddition hook')
    setAddedValue(parseInt(value1) + parseInt(value2))
  }, [value1, value2])

  return [addedValue]
}

export default useAddition
