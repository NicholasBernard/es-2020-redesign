import { useEffect, useState } from 'react'
import axios from 'axios'

import { esAuthToken, handleError, ROOT_URL } from '../services/api'

const getData = async function({ setData, setLoading, url }) {
  try {
    setLoading(true)
    const response = await axios.get(`${ROOT_URL}${url}`, esAuthToken())
    setData(response.data)
  } catch (error) {
    handleError(error)
  } finally {
    setLoading(false)
  }
}

export const useApiGet = url => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    getData({ setData, setLoading, url })
  }, [url])

  return [loading, data, setData]
}

export const useApiGetById = (url, id) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    // '0' is a new record and so no GET is needed
    if (id !== '0') {
      getData({ setData, setLoading, url })
    }
  }, [url, id])

  return [loading, data, setData]
}
