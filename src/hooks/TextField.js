import React, { useState } from 'react'
import { TextField } from '@material-ui/core'

export const SimpleTextField = ({ value, setValue, disabled }) => {
  return (
    <TextField
      disabled={disabled}
      value={value}
      onChange={e => setValue(e.target.value)}
    />
  )
}

export const useTextField = initialValue => {
  const [value, setValue] = useState(initialValue)
  return { value, setValue }
}
