import React from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core'
import { useLocalStorage } from '../hooks'
import { ThemeContext } from '../context'
import { esDark, esLight, steelBlue } from '../constants'

const EsThemeProvider = ({ children }) => {
  const [selectedThemeObject, setSelectedThemeObject] = useLocalStorage(
    'enriching-students:selected-theme',
    JSON.stringify(esLight)
  )
  const [selectedThemeString, setSelectedThemeString] = useLocalStorage(
    'enriching-students:selected-theme-title',
    'esLight'
  )

  const handleThemeChange = value => {
    setSelectedThemeString(value)

    switch (value) {
      case 'esDark':
        setSelectedThemeObject(JSON.stringify(esDark))
        break
      case 'esLight':
        setSelectedThemeObject(JSON.stringify(esLight))
        break
      case 'steelBlue':
        setSelectedThemeObject(JSON.stringify(steelBlue))
        break
      default:
        setSelectedThemeObject(JSON.stringify(esLight))
    }
  }

  const parsedTheme = JSON.parse(selectedThemeObject)
  const themeToUse = createMuiTheme({
    palette: {
      primary: {
        main: parsedTheme.palette.primary.main
      },
      secondary: {
        main: parsedTheme.palette.secondary.main
      },
      error: { main: parsedTheme.palette.error.main },
      success: {
        main: parsedTheme.palette.success.main,
        dark: parsedTheme.palette.success.dark
      },
      nav: {
        background: parsedTheme.palette.nav.background,
        text: parsedTheme.palette.nav.text
      },
      header: { background: parsedTheme.palette.header.background },
      background: {
        default: parsedTheme.palette.background.default,
        paper: parsedTheme.palette.background.paper
      },
      link: { main: parsedTheme.palette.link.main },
      heading: { list: parsedTheme.palette.heading.list },
      type: parsedTheme.palette.type
    },
    typography: {
      fontSize: parsedTheme.typography.fontSize
    }
  })

  return (
    <MuiThemeProvider theme={themeToUse}>
      <ThemeContext.Provider
        value={{
          handleThemeChange: handleThemeChange,
          selectedThemeString: selectedThemeString
        }}>
        {children}
      </ThemeContext.Provider>
    </MuiThemeProvider>
  )
}

export default EsThemeProvider
