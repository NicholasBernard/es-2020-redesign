export const urls = {}

const ESU_ROOT_URL = 'https://www.enrichingstudents.com/university'

export const esuUrls = {
  MANAGE_DEPARTMENTS: `${ESU_ROOT_URL}`
}
