export const headerHeight = '81px'
export const headerHeightXs = '81px'
export const navigationHeight = `100% - ${headerHeight}`
export const navigationWidth = '240px'
export const navigationWidthXs = '300px'
