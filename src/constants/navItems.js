export const navIcon = theme => ({
  color: theme.palette.nav.text,

  [theme.breakpoints.up('md')]: {
    width: 81,

    display: 'flex',
    justifyContent: 'flex-end',

    marginRight: 10,
    paddingRight: 10
  }
})

export const navText = theme => ({
  color: theme.palette.nav.text,
  fontSize: 18
})
