import React from 'react'
import { CssBaseline } from '@material-ui/core'
import { Layout } from './components/layout'

import { EsThemeProvider } from './providers'

const App = () => {
  return (
    <EsThemeProvider>
      <CssBaseline />
      <Layout />
    </EsThemeProvider>
  )
}

export default App
