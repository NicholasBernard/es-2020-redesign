import React from 'react'
import { Link } from '@reach/router'
import { ListItem, ListItemText, makeStyles } from '@material-ui/core'
import { ArrowRight } from '@material-ui/icons'
import { flexRow, navText } from '../../constants'

const useStyles = makeStyles(theme => ({
  icon: {
    color: theme.palette.nav.text
  },
  link: {
    ...flexRow,

    textDecoration: 'none'
  },
  listItem: { paddingLeft: theme.spacing(7) },
  text: {
    ...navText(theme),
    fontSize: 16
  }
}))

const NavLink = ({ label, to }) => {
  const classes = useStyles()
  return (
    <Link className={classes.link} to={to}>
      <ListItem button key={label} className={classes.listItem}>
        <ArrowRight className={classes.icon} />
        <ListItemText
          classes={{ primary: classes.text }}
          primary={label}
          style={{ paddingLeft: 0 }}
        />
      </ListItem>
    </Link>
  )
}

export default NavLink
