import React from 'react'
import { Link } from '@reach/router'
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles
} from '@material-ui/core'
import { flexRow, navIcon } from '../../constants'

const useStyles = makeStyles(theme => ({
  icon: {
    ...navIcon(theme)
  },
  link: {
    ...flexRow,

    textDecoration: 'none'
  },
  text: {
    color: theme.palette.nav.text,
    fontSize: 18
  }
}))

const NavLink = ({ label, Icon, to }) => {
  const classes = useStyles()
  return (
    <Link className={classes.link} to={to}>
      <ListItem button key={label} style={{ paddingLeft: 0 }}>
        <ListItemIcon className={classes.icon}>{Icon}</ListItemIcon>
        <ListItemText
          classes={{ primary: classes.text }}
          primary={label}
          style={{ paddingLeft: 0 }}
        />
      </ListItem>
    </Link>
  )
}

export default NavLink
