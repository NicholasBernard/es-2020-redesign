import React from 'react'
import { Grid, makeStyles } from '@material-ui/core'
import { Subheading, SubheadingText } from '../shared'

const useStyles = makeStyles({
  centered: { textAlign: 'center' },
  number: { textAlign: 'right' }
})

const DepartmentsSubheading = ({ sortByDepartments }) => {
  const classes = useStyles()
  return (
    <Subheading>
      <Grid item sm={4} md={3}>
        <SubheadingText
          onClick={() => sortByDepartments()}
          isSortable
          text='Department'
        />
      </Grid>
      <Grid item sm={4} md={2} className={classes.number}>
        <SubheadingText text='Users In Department' />
      </Grid>
      <Grid item sm={3} md={2} className={classes.centered}>
        <SubheadingText text='Active' />
      </Grid>
    </Subheading>
  )
}

export default DepartmentsSubheading
