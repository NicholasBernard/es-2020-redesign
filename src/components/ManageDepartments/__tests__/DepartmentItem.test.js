import React from 'react'
import { cleanup, render } from '@testing-library/react'
import DepartmentItem from '../DepartmentItem'

const initialProps = {
  departmentId: 1,
  description: 'My Test Department 1',
  isHomerooomDepartment: false,
  numberOfUsers: 10,
  isActive: true,
  schoolId: 1
}

describe('<DepartmentItem />', () => {
  afterEach(() => {
    cleanup()
    jest.clearAllMocks()
  })

  const setup = propsFromTest => {
    return render(<DepartmentItem {...initialProps} {...propsFromTest} />)
  }

  test('renders correctly', () => {
    const { getByTestId } = setup()

    expect(getByTestId('department-description').value).toBe(
      initialProps.description
    )
    expect(getByTestId('number-of-users').textContent).toBe(
      initialProps.numberOfUsers.toString()
    )
    expect(getByTestId('is-active').checked).toBe(initialProps.isActive)
  })
})
