import React, { useEffect, useReducer, useState } from 'react'
import { DepartmentItem, Subheading } from '../ManageDepartments'
import { List } from '../shared'
import { sortByCollatingField } from '../../helpers/array'

const DepartmentsList = ({ data }) => {
  const [sortAscValue, setSortAscValue] = useState(true)
  const [departments, dispatchDepartments] = useReducer((state, action) => {
    switch (action.type) {
      case 'DELETE':
        return [...state].filter(
          item => item.departmentId !== action.departmentId
        )
      case 'SORT':
        return [...state].sort(
          sortByCollatingField(sortAscValue, 'description')
        )
      default:
        return [...state].sort((a, b) => a - b)
    }
  }, data)

  useEffect(() => {
    dispatchDepartments({ type: 'SORT' })
  }, [sortAscValue])

  const fakeDeleteFunction = departmentId => {
    dispatchDepartments({ type: 'DELETE', departmentId })
  }

  return (
    <>
      <Subheading sortByDepartments={() => setSortAscValue(!sortAscValue)} />
      <List
        data={departments}
        Component={DepartmentItem}
        emptyArrayMessage='No Departments'
        keyValue='departmentId'
        deleteDepartment={fakeDeleteFunction}
      />
    </>
  )
}

export default DepartmentsList
