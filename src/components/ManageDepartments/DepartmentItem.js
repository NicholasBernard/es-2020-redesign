import React, { useRef } from 'react'
import { Grid, MenuItem, makeStyles } from '@material-ui/core'
import {
  ActiveIcon,
  DeleteModal,
  HiddenLabel,
  InternalLink,
  Menu
} from '../shared'

const useStyles = makeStyles(theme => ({
  centered: { textAlign: 'center' },
  gridItem: {
    [theme.breakpoints.only('xs')]: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    }
  },
  number: { textAlign: 'right', paddingRight: theme.spacing() },
  root: {
    padding: theme.spacing(),
    borderBottom: '1px solid black'
  }
}))

const DepartmentItem = ({
  deleteDepartment,
  departmentId,
  description,
  isActive,
  isHomeroomDepartment,
  numberOfUsers
}) => {
  const classes = useStyles()
  const ref = useRef('deleteModal')
  return (
    <Grid
      container
      className={classes.root}
      alignItems='center'
      data-testid='department-item'>
      <Grid xs={12} sm={4} md={3} item className={classes.gridItem}>
        <HiddenLabel>Department:</HiddenLabel>
        {description}
      </Grid>
      <Grid
        xs={12}
        sm={4}
        md={2}
        item
        className={(classes.gridItem, classes.number)}
        data-testid='number-of-users'>
        <HiddenLabel>Users In Department:</HiddenLabel>
        {numberOfUsers}
      </Grid>
      <Grid item sm={3} md={2} className={(classes.gridItem, classes.centered)}>
        <HiddenLabel>Is Active:</HiddenLabel>
        <ActiveIcon active={isActive} />
      </Grid>
      <Grid item sm={1} md={2}>
        <Menu>
          {isHomeroomDepartment || numberOfUsers > 0 ? null : (
            <DeleteModal
              label='Delete Department'
              ref={ref}
              confirmDeleteCallback={() => deleteDepartment(departmentId)}
              confirmDeleteLabel='Delete Department'
              confirmDeleteMessage='Are you sure you want to delete this department?'
              itemName={description}
            />
          )}
          <MenuItem>
            <InternalLink
              noStyling
              text='Edit Department'
              to={`/createDepartment/${departmentId}`}
            />
          </MenuItem>
        </Menu>
      </Grid>
    </Grid>
  )
}

export default DepartmentItem
