import React from 'react'
import { Router } from '@reach/router'
import { makeStyles } from '@material-ui/core/styles'
import { headerHeight, headerHeightXs } from '../../constants'

import {
  CreateDepartmentScreen,
  DashboardScreen,
  GlobalErrorHandling,
  ManageDepartmentsScreen,
  TakeAttendanceScreen,
  TestScreen,
  UserSettingsScreen
} from '../screens'

const useStyles = makeStyles(theme => ({
  content: {
    flexGrow: 1,
    paddingRight: theme.spacing(3),
    paddingLeft: theme.spacing(3),

    top: headerHeight,
    position: 'relative',
    paddingBottom: theme.spacing(),
    paddingTop: theme.spacing(2),

    [theme.breakpoints.only('xs')]: {
      top: headerHeightXs
    },

    [theme.breakpoints.up('md')]: {
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2)
    }
  }
}))

const Main = () => {
  const classes = useStyles()
  return (
    <main className={classes.content}>
      <Router>
        <CreateDepartmentScreen path='/createDepartment/:departmentId' />
        <DashboardScreen path='/' />
        <GlobalErrorHandling path='/globalErrorHandling' />
        <ManageDepartmentsScreen path='/manageDepartments' />
        <TakeAttendanceScreen path='/takeAttendance' />
        <TestScreen path='/testScreen' />
        <UserSettingsScreen path='/userSettings' />
      </Router>
    </main>
  )
}

export default Main
