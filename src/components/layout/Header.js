import React from 'react'
import { AppBar, IconButton, Typography, makeStyles } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import { flexRow, headerHeight } from '../../constants'

const useStyles = makeStyles(theme => ({
  appBar: {
    backgroundColor: theme.palette.header.background,
    height: 81,

    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',

      width: '100%',
      zIndex: 1000
    }
  },
  headerTitle: {
    [theme.breakpoints.down('md')]: {
      marginLeft: 0
    },

    marginLeft: 10,
    color: theme.palette.nav.text
  },
  icon: {
    fontSize: 40
  },
  logoContainer: {
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    },

    height: headerHeight,
    width: headerHeight,

    backgroundColor: theme.palette.primary.main,

    paddingRight: theme.spacing(2)
  },
  menuButton: {
    color: theme.palette.nav.text,
    marginRight: 0,

    [theme.breakpoints.up('lg')]: {
      display: 'none'
    }
  },
  pageInfoContainer: {
    ...flexRow,
    flex: 1
  }
}))

const Header = ({ headerTitle, mobileMenuOpen, setMobileMenuOpen }) => {
  const classes = useStyles()
  return (
    <AppBar position='fixed' className={classes.appBar}>
      <div className={classes.pageInfoContainer}>
        <div className={classes.logoContainer} />
        <IconButton
          color='inherit'
          aria-label='Open drawer'
          onClick={() => setMobileMenuOpen(!mobileMenuOpen)}
          className={classes.menuButton}>
          <MenuIcon className={classes.icon} />
        </IconButton>
        <Typography variant='h5' className={classes.headerTitle} noWrap>
          {headerTitle}
        </Typography>
      </div>
    </AppBar>
  )
}

export default Header
