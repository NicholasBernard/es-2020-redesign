import React from 'react'
import {
  Collapse,
  Drawer,
  Hidden,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles
} from '@material-ui/core'
import { Dashboard, ExpandLess, ExpandMore } from '@material-ui/icons'
import {
  headerHeight,
  navIcon,
  navText,
  navigationWidth,
  navigationWidthXs
} from '../../constants'

import { NavMenuLink, SubNavMenuLink } from '../Navigation'
import { useLocalStorage } from '../../hooks'

const useStyles = makeStyles(theme => ({
  container: {
    paddingLeft: theme.spacing(),

    [theme.breakpoints.down('sm')]: {
      top: headerHeight,
      position: 'absolute',
      width: '100%'
    },

    [theme.breakpoints.up('lg')]: {
      paddingLeft: 0
    }
  },
  nav: {
    [theme.breakpoints.up('sm')]: {
      width: navigationWidth,
      zIndex: 0
    }
  },
  drawerPaper: {
    backgroundColor: theme.palette.nav.background,
    width: navigationWidthXs,

    [theme.breakpoints.up('md')]: {
      width: navigationWidth
    },

    [theme.breakpoints.up('lg')]: {
      height: `calc(100vh - ${headerHeight})`,
      top: headerHeight,

      paddingTop: theme.spacing()
    }
  },
  icon: {
    ...navIcon(theme)
  },
  mobileLogo: {
    height: headerHeight,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  scrollbars: {
    height: `calc(100vh - ${headerHeight})`,
    width: '100%'
  },
  text: {
    ...navText(theme)
  }
}))

const Navigation = ({ container, mobileMenuOpen, setMobileMenuOpen }) => {
  const classes = useStyles()
  const [adminMenuOpen, setAdminMenuOpen] = useLocalStorage(
    'enriching-students:admin-menu-open',
    false
  )

  const handleAdminClick = () => {
    setAdminMenuOpen(!adminMenuOpen)
  }

  const drawer = (
    <div>
      <NavMenuLink label='Dashboard' Icon={<Dashboard />} to='/' />
      <NavMenuLink label='Test Screen' Icon={<Dashboard />} to='/testScreen' />
      <NavMenuLink
        label='User Settings'
        Icon={<Dashboard />}
        to='/userSettings'
      />
      <ListItem button style={{ paddingLeft: 0 }} onClick={handleAdminClick}>
        <ListItemIcon className={classes.icon}>
          <Dashboard />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.text }}
          primary='Admin'
          style={{ paddingLeft: 0 }}
        />
        <ListItemIcon className={classes.icon}>
          {adminMenuOpen ? <ExpandMore /> : <ExpandLess />}
        </ListItemIcon>
      </ListItem>
      <Collapse in={adminMenuOpen}>
        <SubNavMenuLink label='Manage Departments' to='/manageDepartments' />
      </Collapse>
    </div>
  )

  return (
    <nav className={classes.nav}>
      {/* For mobile devices */}
      <Hidden smUp implementation='css'>
        <Drawer
          container={container}
          variant='temporary'
          anchor='left'
          open={mobileMenuOpen}
          onClose={() => setMobileMenuOpen(false)}
          classes={{
            paper: classes.drawerPaper
          }}>
          <div className={classes.mobileLogo}>Logo Goes Here</div>
        </Drawer>
      </Hidden>
      {/* For larger devices */}
      <Hidden mdDown implementation='css'>
        <Drawer
          classes={{
            paper: classes.drawerPaper
          }}
          variant='permanent'
          open>
          {drawer}
        </Drawer>
      </Hidden>
    </nav>
  )
}

export default Navigation
