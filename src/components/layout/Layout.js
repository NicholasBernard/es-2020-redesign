import React, { useState } from "react";
import { makeStyles } from "@material-ui/core";
import { HeaderTitleContext } from "../../context";
import Header from "./Header";
import Main from "./Main";
import Nav from "./Nav";

const useStyles = makeStyles(theme => ({
  root: {
    [theme.breakpoints.up("lg")]: {
      display: "flex"
    }
  },
  scrollbars: {
    height: "100vh !important",
    width: "100%"
  }
}));

const Layout = () => {
  const [headerTitle, setHeaderTitle] = useState("Header title from App");
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Header
        headerTitle={headerTitle}
        mobileMenuOpen={mobileMenuOpen}
        setMobileMenuOpen={setMobileMenuOpen}
      />
      <Nav
        mobileMenuOpen={mobileMenuOpen}
        setMobileMenuOpen={setMobileMenuOpen}
      />
      <HeaderTitleContext.Provider value={{ setHeaderTitle: setHeaderTitle }}>
        <Main />
      </HeaderTitleContext.Provider>
    </div>
  );
};

export default Layout;
