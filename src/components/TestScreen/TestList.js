import React, { useEffect, useReducer } from 'react'
import { DepartmentItem } from '../ManageDepartments'
import { List } from '../shared'

const TestList = ({ departmentsFromServer, sortAscValue }) => {
  const REDUCER = (state, action) => {
    const collator = new Intl.Collator('en', {
      numeric: true,
      sensitivity: 'base'
    })

    switch (action.type) {
      case 'DELETE':
        return [...state].filter(
          item => item.departmentId !== action.departmentId
        )
      case 'SORT_ASC':
        return [...state].sort((a, b) =>
          collator.compare(a.description, b.description)
        )
      case 'SORT_DESC':
        return [...state].sort((a, b) =>
          collator.compare(b.description, a.description)
        )
      default:
        return [...state].sort((a, b) => a - b)
    }
  }

  const [departments, dispatchDepartments] = useReducer(
    REDUCER,
    departmentsFromServer
  )

  useEffect(() => {
    if (sortAscValue) {
      dispatchDepartments({ type: 'SORT_ASC' })
    } else {
      dispatchDepartments({ type: 'SORT_DESC' })
    }
  }, [sortAscValue])

  const fakeDeleteFunction = departmentId => {
    dispatchDepartments({ type: 'DELETE', departmentId })
  }

  return (
    <List
      data={departments}
      Component={DepartmentItem}
      emptyArrayMessage='No Departments'
      keyValue='departmentId'
      deleteDepartment={fakeDeleteFunction}
    />
  )
}

export default TestList
