import React, { useState } from 'react'
import { useAddition, useHeaderTitle } from '../../hooks'

const setDocumentTitle = title => {
  document.title = `Enriching Students - ${title}`
}

const TakeAttendanceScreen = () => {
  const [titleTest, setTitleTest] = useState('Initial Value')
  const [testValue1, setTestValue1] = useState(1)
  const [testValue2, setTestValue2] = useState(1)

  const [addedValue] = useAddition(testValue1, testValue2)

  setDocumentTitle('Take Attendance')

  useHeaderTitle('Take Attendance')

  return (
    <div>
      <p>TakeAttendanceScreen</p>
      <input onChange={e => setTitleTest(e.target.value)} value={titleTest} />
      <input onChange={e => setTestValue1(e.target.value)} value={testValue1} />
      <input onChange={e => setTestValue2(e.target.value)} value={testValue2} />
      <span>{addedValue}</span>
    </div>
  )
}

export default TakeAttendanceScreen
