import React from 'react'
import axiosMock from 'axios'
import {
  cleanup,
  render,
  wait,
  waitForElement,
  waitForElementToBeRemoved
} from '@testing-library/react'
import ManageDepartments from '../ManageDepartments'

import { esAuthToken, ROOT_URL } from '../../../services/api'

describe('<ManageDepartments />', () => {
  afterEach(() => {
    cleanup()
    jest.clearAllMocks()
  })

  const setup = () => {
    return render(<ManageDepartments />)
  }

  test('a message is displayed when there are no departments', async () => {
    axiosMock.get.mockResolvedValueOnce({ data: {} })
    const { getByTestId } = setup()

    await waitForElementToBeRemoved(() => getByTestId('loading-component'))

    const emptyArrayMessage = getByTestId('empty-array-message')

    expect(emptyArrayMessage).toBeTruthy()
    expect(emptyArrayMessage.textContent).toBe('No Departments')
  })

  test('renders the correct number of components when data is passed in', async () => {
    axiosMock.get.mockResolvedValueOnce({
      data: [
        {
          departmentId: 1,
          departmentName: 'test dept 1'
        },
        {
          departmentId: 2,
          departmentName: 'test dept 2'
        },
        {
          departmentId: 3,
          departmentName: 'test dept 3'
        }
      ]
    })

    const { getAllByTestId, getByTestId } = setup()

    await waitForElementToBeRemoved(() => getByTestId('loading-component'))

    const departmentItems = getAllByTestId('department-item')
    expect(departmentItems.length).toBe(3)
  })

  test('initial GET is called once with correct args', async () => {
    axiosMock.get.mockResolvedValueOnce({ data: [] })

    setup()
    await wait()

    expect(axiosMock.get).toHaveBeenCalledTimes(1)
    expect(axiosMock.get).toHaveBeenCalledWith(
      `${ROOT_URL}departments`,
      esAuthToken()
    )
  })
})
