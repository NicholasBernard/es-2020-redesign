import React from 'react'
import axiosMock from 'axios'
import {
  cleanup,
  render,
  wait,
  waitForElementToBeRemoved
} from '@testing-library/react'
import TestScreen from '../TestScreen'
import { esAuthToken, ROOT_URL } from '../../../services/api'

describe('<TestScreen />', () => {
  afterEach(() => {
    cleanup()
    jest.clearAllMocks()
  })

  const setup = () => {
    return render(<TestScreen />)
  }

  test('initial GET is called once with correct args', async () => {
    axiosMock.get.mockResolvedValueOnce({
      data: [
        {
          departmentId: 1,
          description: 'Math',
          isHomeroomDepartment: false,
          isActive: true,
          numberOfUsers: 1000,
          schoolId: 1
        }
      ]
    })

    axiosMock.get.mockResolvedValueOnce({
      data: [
        {
          hasBeenUsed: true,
          appointmentTypeId: 0,
          description: 'string',
          isActive: true,
          isDefault: true,
          schoolId: 0
        }
      ]
    })

    axiosMock.get.mockResolvedValueOnce({
      data: [
        {
          firstName: 'Test',
          lastName: 'Student',
          schoolId: 1,
          userId: 1
        }
      ]
    })

    setup()
    await wait()

    expect(axiosMock.get).toHaveBeenCalledTimes(3)
    expect(axiosMock.get).toHaveBeenCalledWith(
      `${ROOT_URL}departments`,
      esAuthToken()
    )

    expect(axiosMock.get).toHaveBeenCalledWith(
      `${ROOT_URL}appointmentTypes`,
      esAuthToken()
    )

    expect(axiosMock.get).toHaveBeenCalledWith(
      `${ROOT_URL}unscheduledstudents`,
      esAuthToken()
    )
  })

  test('2 api calls made', async () => {
    axiosMock.get.mockResolvedValueOnce({
      data: [
        {
          departmentId: 1,
          description: 'Math',
          isHomeroomDepartment: false,
          isActive: true,
          numberOfUsers: 1000,
          schoolId: 1
        }
      ]
    })

    axiosMock.get.mockResolvedValueOnce({
      data: [
        {
          hasBeenUsed: true,
          appointmentTypeId: 1,
          description: 'Appointment Type 1',
          isActive: true,
          isDefault: true,
          schoolId: 1
        },
        {
          hasBeenUsed: true,
          appointmentTypeId: 2,
          description: 'Appointment Type 2',
          isActive: true,
          isDefault: true,
          schoolId: 1
        }
      ]
    })

    axiosMock.get.mockResolvedValueOnce({
      data: [
        {
          firstName: 'Test',
          lastName: 'Student',
          schoolId: 1,
          userId: 1
        }
      ]
    })

    const { getAllByTestId, getByTestId } = setup()

    await waitForElementToBeRemoved(() => getByTestId('loading-component'))

    const departmentItems = getAllByTestId('department-item')
    expect(departmentItems.length).toBe(1)

    const appointmentTypeItems = getAllByTestId('appointment-type-item')
    expect(appointmentTypeItems.length).toBe(2)

    const unscheduleStudentItems = getAllByTestId('unscheduled-student-item')
    expect(unscheduleStudentItems.length).toBe(1)
  })
})
