import React from 'react'

import { Text } from '../shared'

const GlobalErrorHandling = () => {
  return <Text>Something went wrong</Text>
}

export default GlobalErrorHandling
