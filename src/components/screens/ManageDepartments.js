import React from 'react'
import { Grid } from '@material-ui/core'
import { DepartmentsList } from '../ManageDepartments'
import { useApiGet, useHeaderTitle } from '../../hooks'
import {
  LinkButton,
  Loading,
  Paper,
  PaperTitle,
  Text,
  UserManual
} from '../shared'
import { newRecordId, esuUrls } from '../../constants'

const FAKE_DEPARTMENTS = [
  {
    departmentId: 0,
    description: 'Test Department 1',
    isActive: true,
    isHomeroomDepartment: true,
    numberOfUsers: 15
  },
  {
    departmentId: 1,
    description: 'Test Department 2',
    isActive: true,
    isHomeroomDepartment: true,
    numberOfUsers: 15
  },
  {
    departmentId: 2,
    description: 'Test Department 3',
    isActive: true,
    isHomeroomDepartment: true,
    numberOfUsers: 15
  }
]

const ManageDepartments = () => {
  useHeaderTitle('Manage Departments')
  //const [loading, departments] = useApiGet('departments')

  //if (loading) return <Loading text='Loading Department Data!' />

  return (
    <>
      <Grid container justify='space-between'>
        <Grid item>
          <UserManual href={`${esuUrls.MANAGE_DEPARTMENTS}`} />
        </Grid>
        <Grid item>
          <LinkButton
            label='Create Department'
            to={`/createDepartment/${newRecordId}`}
          />
        </Grid>
      </Grid>
      <Text>
        Departments can only be deactivated/deleted if there are no staffers
        assigned to the department. You may change the name of the homeroom
        department, but it cannot be deactivated or deleted.
      </Text>
      <Paper>
        <PaperTitle>Departments</PaperTitle>
        <DepartmentsList /* data={departments} */ data={FAKE_DEPARTMENTS} />
      </Paper>
    </>
  )
}

export default ManageDepartments
