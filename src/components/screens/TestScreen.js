import React from 'react'
import { useHeaderTitle } from '../../hooks'
import TestStepper from '../TestScreen/TestStepper'

const TestScreen = () => {
  useHeaderTitle('Test Screen')

  return <TestStepper />
}

export default TestScreen
