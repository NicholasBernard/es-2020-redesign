import React, { useContext } from 'react'
import { ThemeContext } from '../../context/context'

const UserSettings = () => {
  const { handleThemeChange, selectedThemeString } = useContext(ThemeContext)

  return (
    <div>
      <select
        onChange={e => handleThemeChange(e.target.value)}
        value={selectedThemeString}>
        <option value='-1'>(select)</option>
        <option value='esLight'>Default</option>
        <option value='esDark'>Dark</option>
        <option value='steelBlue'>Steel Blue</option>
      </select>
    </div>
  )
}

export default UserSettings
