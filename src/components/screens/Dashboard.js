import React from 'react'
import { useHeaderTitle } from '../../hooks'
import { Button } from '../shared'

const DashboardScreen = () => {
  useHeaderTitle('Dashboard')

  return (
    <div>
      <p>DashboardScreen</p>
      <div
        style={{
          width: 250,
          display: 'flex',
          justifyContent: 'space-between'
        }}>
        <Button
          color='primary'
          label='Test Button'
          onClick={() => console.log('clicked')}
        />
        <Button
          color='secondary'
          label='Test Button'
          onClick={() => console.log('clicked')}
        />
      </div>
    </div>
  )
}

export default DashboardScreen
