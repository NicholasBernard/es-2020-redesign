import React from 'react'
import axios from 'axios'
import { navigate } from '@reach/router'
import * as yup from 'yup'
import { Formik, Form, Field } from 'formik'
import {
  CheckboxFormField,
  Loading,
  PageTitle,
  SubmitButton,
  TextFormField
} from '../shared'
import { makeStyles } from '@material-ui/core'
import { useApiGetById } from '../../hooks'
import { esAuthToken, ROOT_URL } from '../../services/api'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  }
}))

const schema = yup.object({
  departmentDescription: yup
    .string()
    .required('Department name is required')
    .max(150, 'Department cannot exceed 150 characters'),
  isActive: yup.bool()
})

const CreateDepartment = props => {
  const classes = useStyles()
  const { departmentId } = props

  const [loading, department] = useApiGetById(
    `departments/${departmentId}`,
    departmentId
  )

  console.log(department)

  const handleSave = formValues => {
    const obj = {
      departmentId: parseInt(props.departmentId),
      departmentDescription: formValues.departmentDescription,
      isActive: formValues.isActive
    }

    axios
      .post(`${ROOT_URL}departments`, obj, esAuthToken())
      .then(() => navigate('/manageDepartments'))
      .catch(error => console.log(error))
  }

  if (loading) return <Loading text='Loading Department Info' />

  return (
    <div className={classes.root}>
      <PageTitle text='Create a Department' />
      <Formik
        initialValues={{
          departmentDescription:
            departmentId === '0' ? '' : department.description,
          isActive: departmentId === '0' ? true : department.isActive
        }}
        onSubmit={values => handleSave(values)}
        validationSchema={schema}>
        <Form>
          <Field
            label='Add New Department'
            name='departmentDescription'
            component={TextFormField}
          />
          <Field
            label='Is Active'
            name='isActive'
            component={CheckboxFormField}
          />
          <SubmitButton label='Save' />
        </Form>
      </Formik>
    </div>
  )
}

export default CreateDepartment
