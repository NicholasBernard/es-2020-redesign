import React from 'react'

import { Hidden } from '@material-ui/core'
import Text from './Text'

const HiddenLabel = ({ children }) => {
  return (
    <Hidden smUp>
      <Text>{children}</Text>{' '}
    </Hidden>
  )
}

export default HiddenLabel
