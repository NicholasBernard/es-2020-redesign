import React from 'react'
import { CircularProgress, Typography, withStyles } from '@material-ui/core'

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    height: 'calc(100vh - 115px)'
  },
  fullscreen: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    height: '90vh',
    width: '100vw'
  },
  login: {
    color: 'white'
  },
  loginTypography: {
    color: 'white'
  },
  progress: {
    color: theme.palette.primary.main
  },
  typography: {
    marginTop: theme.spacing(2)
  }
})

const Loading = ({ classes, fullscreen, login, text }) => {
  return (
    <div
      className={fullscreen ? classes.fullscreen : classes.container}
      data-testid='loading-component'>
      <CircularProgress
        className={login ? classes.login : classes.progress}
        size={80}
      />
      <Typography
        className={login ? classes.loginTypography : classes.typography}
        variant='h4'
        align='center'>
        {text}
      </Typography>
    </div>
  )
}

export default withStyles(styles)(Loading)
