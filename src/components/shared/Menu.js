import React, { useState } from 'react'
import { Button, Menu, makeStyles } from '@material-ui/core'
import { MoreHoriz } from '@material-ui/icons'

const useStyles = makeStyles({
  icon: {
    fontSize: 32
  }
})

const EsMenu = ({ children }) => {
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = useState(null)

  const handleClick = e => {
    setAnchorEl(e.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div>
      <Button onClick={handleClick}>
        <MoreHoriz className={classes.icon} />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        {children}
      </Menu>
    </div>
  )
}

export default EsMenu
