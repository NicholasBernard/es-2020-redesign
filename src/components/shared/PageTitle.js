import React from 'react'
import { Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    //color: theme.palette.primary.main
    color:
      theme.palette.type === 'light'
        ? theme.palette.primary.main
        : theme.palette.secondary.main,
    marginBottom: theme.spacing(2)
  }
}))

const PageTitle = ({ text }) => {
  const classes = useStyles()
  return (
    <Typography className={classes.root} variant='h5'>
      {text}
    </Typography>
  )
}

export default PageTitle
