import { getIn } from 'formik'
import React from 'react'
import { TextField } from '@material-ui/core'

const TextFormField = ({
  dataTestId,
  field,
  form,
  placeholder,
  type,
  ...props
}) => {
  const errorText =
    getIn(form.touched, field.name) && getIn(form.errors, field.name)

  return (
    <TextField
      error={!!errorText}
      helperText={errorText}
      inputProps={{ 'data-testid': `${dataTestId}` }}
      margin='normal'
      placeholder={placeholder}
      style={{ width: 300 }}
      type={type}
      {...field}
      {...props}
    />
  )
}

export default TextFormField
