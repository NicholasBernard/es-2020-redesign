import React from 'react'
import PropTypes from 'prop-types'
import { Button, makeStyles } from '@material-ui/core'
//import Button from '../Button'

const useStyles = makeStyles(theme => ({
  button: {
    fontSize: 13,

    maxWidth: 250,

    paddingTop: 4,
    paddingRight: 10,
    paddingBottom: 4,
    paddingLeft: 10
  }
}))

const SubmitButton = ({ label }) => {
  const classes = useStyles()
  return (
    <Button
      className={classes.button}
      color='primary'
      data-testid='submit-button'
      type='submit'
      variant='contained'>
      {label}
    </Button>
  )
}

SubmitButton.propTypes = {
  label: PropTypes.string.isRequired
}

export default SubmitButton
