import React from 'react'
import { Checkbox } from '@material-ui/core'

const EsCheckbox = ({ checked, dataTestId, name, onChange }) => {
  return (
    <Checkbox
      checked={checked}
      color='primary'
      inputProps={{ 'data-testid': dataTestId }}
      name={name}
      onChange={onChange}
    />
  )
}

export default EsCheckbox
