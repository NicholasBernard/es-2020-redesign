import React from 'react'
import { Paper, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginBottom: theme.spacing(3),
    overflowX: 'auto',

    paddingRight: theme.spacing(),
    paddingLeft: theme.spacing()
  }
}))

const EsPaper = ({ children, overwrite }) => {
  const classes = useStyles()
  return (
    <Paper className={[classes.root, overwrite].join(' ')}>{children}</Paper>
  )
}

export default EsPaper
