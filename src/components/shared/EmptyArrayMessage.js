import React from 'react'

const EmptyArrayMessage = ({ text }) => {
  return <p data-testid='empty-array-message'>{text}</p>
}

export default EmptyArrayMessage
