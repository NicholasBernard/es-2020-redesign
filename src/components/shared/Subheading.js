import React from 'react'
import { Grid, Hidden, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  root: {
    //backgroundColor: '#eee',
    backgroundColor: theme.palette.primary.main,
    padding: theme.spacing()
  }
}))

const Subheading = ({ children }) => {
  const classes = useStyles()
  return (
    <Hidden xsDown>
      <Grid container /* spacing={2} */ className={classes.root}>
        {children}
      </Grid>
    </Hidden>
  )
}

export default Subheading
