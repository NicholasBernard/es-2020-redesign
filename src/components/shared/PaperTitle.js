import React from 'react'
import { Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  paperTitle: {
    paddingTop: theme.spacing(),
    paddingBottom: theme.spacing(),

    color: theme.palette.heading.list,

    fontSize: 20,
    //fontWeight: 'bold'
    fontWeight: 500
  }
}))

const PaperTitle = ({ children }) => {
  const classes = useStyles()
  return <Typography className={classes.paperTitle}>{children}</Typography>
}

export default PaperTitle
