import React from 'react'
import { Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  text: {
    color: '#fff',
    textTransform: 'uppercase'
  },
  sortable: {
    color: '#fff',
    textTransform: 'uppercase',
    textDecoration: 'underline',

    '&:hover': {
      cursor: 'pointer'
    }
  }
}))

const SubheadingText = ({ isSortable, onClick, text }) => {
  const classes = useStyles()

  return (
    <Typography
      className={isSortable ? classes.sortable : classes.text}
      onClick={onClick}>
      {text}
    </Typography>
  )
}

export default SubheadingText
