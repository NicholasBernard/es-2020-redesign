import React from 'react'
import { Button, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  button: {
    fontSize: 13,

    maxWidth: 250,

    paddingTop: 4,
    paddingRight: 10,
    paddingBottom: 4,
    paddingLeft: 10
  }
}))

const EsButton = ({
  className,
  color = 'primary',
  dataTestId,
  disabled,
  label,
  onClick
}) => {
  const classes = useStyles()
  return (
    <Button
      data-testid={dataTestId}
      className={[classes.button, className].join(' ')}
      color={color}
      disabled={disabled}
      onClick={onClick}
      variant='contained'>
      {label}
    </Button>
  )
}

export default EsButton
