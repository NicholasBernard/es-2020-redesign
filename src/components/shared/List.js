import React, { Fragment } from 'react'
import EmptyArrayMessage from './EmptyArrayMessage'

const List = ({
  data,
  Component,
  emptyArrayMessage,
  keyValue,
  ...otherProps
}) => {
  return (
    <Fragment>
      {data.length > 0 ? (
        <Fragment>
          {data.map((item, index) => (
            <Component
              key={item[keyValue]}
              index={index}
              {...item}
              {...otherProps}
            />
          ))}
        </Fragment>
      ) : (
        <EmptyArrayMessage text={emptyArrayMessage} />
      )}
    </Fragment>
  )
}

export default List
