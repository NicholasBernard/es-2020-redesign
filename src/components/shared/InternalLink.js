import React from 'react'
import { Link } from '@reach/router'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  incognito: {
    //color: 'black',
    color: theme.palette.text.primary,
    textDecoration: 'none'
  },
  link: {
    color: 'blue',

    '&:visited': {
      color: 'blue'
    }
  }
}))

const InternalLink = ({ noStyling, text, to }) => {
  const classes = useStyles()
  return (
    <Link className={noStyling ? classes.incognito : classes.link} to={to}>
      {text}
    </Link>
  )
}

export default InternalLink
