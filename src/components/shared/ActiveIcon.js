import React from 'react'
import { makeStyles } from '@material-ui/core'
import { Check, Close } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  check: {
    color: theme.palette.success.main
  },
  close: {
    color: theme.palette.error.main
  }
}))

const ActiveIcon = ({ active }) => {
  const classes = useStyles()
  return (
    <>
      {active ? (
        <Check className={classes.check} />
      ) : (
        <Close className={classes.close} />
      )}
    </>
  )
}

export default ActiveIcon
