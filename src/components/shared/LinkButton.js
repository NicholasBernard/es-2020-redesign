import React from 'react'
import { Link } from '@reach/router'
import { makeStyles } from '@material-ui/core'
import Button from './Button'

const useStyles = makeStyles(theme => ({
  button: {
    backgroundColor: theme.palette.success.main,

    '&:hover': {
      backgroundColor: theme.palette.success.dark
    }
  },
  link: {
    textDecoration: 'none'
  }
}))

const LinkButton = ({ label, to }) => {
  const classes = useStyles()

  return (
    <Link className={classes.link} to={to}>
      <Button className={classes.button} label={label} />
    </Link>
  )
}

export default LinkButton
