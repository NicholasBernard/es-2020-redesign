import React from 'react'
import { Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  text: {
    color: theme.palette.primary.main,
    marginBottom: theme.spacing(),
    fontSize: 21,
    fontWeight: 300
  }
}))

const SectionTitle = ({ text, overwriteClass }) => {
  const classes = useStyles()
  return (
    <Typography className={[classes.text, overwriteClass].join(' ')}>
      {text}
    </Typography>
  )
}

export default SectionTitle
