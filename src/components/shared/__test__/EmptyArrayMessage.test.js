import React from 'react'
import EmptyArrayMessage from '../EmptyArrayMessage'
import { cleanup, render } from '@testing-library/react'

describe('<EmptyArrayMessage />', () => {
  afterEach(cleanup)

  test('renders correctly', () => {
    const { getAllByTestId } = render(
      <EmptyArrayMessage text='Test Empty Message' />
    )

    expect(getAllByTestId('empty-array-message').length).toBe(1)
  })
})
