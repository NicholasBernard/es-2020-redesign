import React from 'react'
import Loading from '../Loading'
import { cleanup, render } from '@testing-library/react'

describe('<Loading />', () => {
  afterEach(cleanup)

  test('renders correctly', () => {
    const { getAllByTestId, getByTestId, getByText } = render(
      <Loading text='Loading Test' />
    )

    expect(getAllByTestId('loading-component').length).toBe(1)

    expect(getByTestId('loading-component')).toBeTruthy()
    expect(getByText('Loading Test')).toBeTruthy()
  })
})
