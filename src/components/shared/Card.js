import React from 'react'
import { Card, CardContent, Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  card: {
    minWidth: 275
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: theme.palette.primary.main,
    paddingTop: theme.spacing(),
    paddingLeft: theme.spacing(2)
  }
}))

const SimpleCard = ({ children, title }) => {
  const classes = useStyles()

  return (
    <Card className={classes.card}>
      <Typography className={classes.title}>{title}</Typography>
      <CardContent>{children}</CardContent>
    </Card>
  )
}

export default SimpleCard
