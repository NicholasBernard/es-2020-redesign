import React from 'react'
import { Info } from '@material-ui/icons'

import { Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  a: {
    display: 'flex',
    //color: 'blue'
    color: theme.palette.link.main
  },
  icon: {
    //color: '#000',
    color: theme.palette.text.primary,
    marginRight: theme.spacing()
  }
}))

const UserManual = ({ href }) => {
  const classes = useStyles()
  return (
    <a className={classes.a} href={href} target='__blank'>
      <Info className={classes.icon} />
      <Typography>User Manual</Typography>
    </a>
  )
}

export default UserManual
