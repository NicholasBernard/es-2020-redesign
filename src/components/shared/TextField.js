import React from 'react'
import { TextField, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  textfield: {
    width: 250,
    marginTop: theme.spacing(),
    marginBottom: theme.spacing(),

    [theme.breakpoints.down('xs')]: {
      width: '100%'
    }
  }
}))

const EsTextField = ({
  dataTestId,
  label,
  name,
  id,
  onChange,
  overwriteClass,
  placeholder,
  setValue,
  type,
  value
}) => {
  const classes = useStyles()
  return (
    <div>
      <TextField
        className={`${classes.textfield} ${overwriteClass}`}
        id={id}
        inputProps={{ 'data-testid': `${dataTestId}` }}
        label={label}
        name={name}
        //onChange={onChange}
        onChange={e => setValue(e.target.value)}
        placeholder={placeholder}
        type={type}
        value={value}
      />
    </div>
  )
}

export default EsTextField
