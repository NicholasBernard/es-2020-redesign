import React from 'react'
import { Typography } from '@material-ui/core'

const Text = ({ children, className }) => {
  return (
    <Typography className={className} paragraph>
      {children}
    </Typography>
  )
}

export default Text
