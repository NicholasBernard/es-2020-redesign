export default {
  delete: jest.fn().mockResolvedValue(1),
  get: jest.fn().mockResolvedValue({ data: {} }),
  post: jest.fn().mockResolvedValue({ data: {} })
}
